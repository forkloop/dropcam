package dropcam

import (
	"os/exec"
	//"bytes"
	//"mime/multipart"
	//"io/ioutil"
	"log"
	//"fmt"
	//"strings"
	//"net/url"
	//"net/http"
)

// notifier interface to send a notification
type Notifier interface {
	Notify(message string, recepite string)
}

type MailgunNotifier struct {
	ApiKey string `default:""`
}

func (m MailgunNotifier) Notify(message string, recepite string) {

	err := exec.Command("curl", "--user", "api:" + m.ApiKey, "https://api.mailgun.net/v2/sandbox127eec0ffde948158292a8986c79715e.mailgun.org/messages", "-F", "from=forkloop@gmail.com", "-F", "to=" + recepite,
					"-F", "subject='Kittens Alert'", "-F", "text='" + message + "'").Run()
	if err != nil {
		log.Println(err)
	}
	//var buf bytes.Buffer
	//w := multipart.NewWriter(&buf)
	//w.WriteField("from", "forkloop@yahoo.com")
	//w.WriteField("to", recepite)
	//w.WriteField("subject", "Kittens Alert")
	//w.WriteField("text", message)
	//body := url.QueryEscape(fmt.Sprintf(
	//    "from=forkloop@yahoo.com&to=%v&subject=%v&text=%v",
	//    recepite, "Kittens Alert", message))
	//req, _ := http.NewRequest("POST", "https://api.mailgun.net/v2/sandbox127eec0ffde948158292a8986c79715e.mailgun.org/messages", &buf)
	//req.Header.Set("Content-Type", w.FormDataContentType())
	//req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	//req.SetBasicAuth("api", m.ApiKey)
	//client := &http.Client{}
	//res, _ := client.Do(req)
	//status, _ := ioutil.ReadAll(res.Body)
	//log.Println(string(status))
}

/*
curl -vvv --user 'api:key' https://api.mailgun.net/v2/sandbox127eec0ffde948158292a8986c79715e.mailgun.org/messages -F from='forkloop@gmail.com' -F to=forkloop@yahoo.com -F subject='Kittens Alert' -F text='Watch out'
*/
