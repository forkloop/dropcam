package dropcam

import (
	"testing"
	"github.com/franela/goblin"
)

func TestNotifier(t *testing.T) {
	g := goblin.Goblin(t)

	g.Describe("Test Drive", func() {
		var foo string
		g.It("foo should be foo", func() {
			foo = "foo"
			g.Assert(foo).Equal("foo")
		})

		g.It("foo should be foo", func() {
			foo = "bar"
			g.Assert(foo).Equal("bar")
		})

		//g.It("foo should not be bar")
	})
}
