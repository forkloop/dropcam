package dropcam

import (
	"os"
	"bytes"
	"log"
	"net/http"
	"path"
	"mime/multipart"
)

var (
	user string
	pass string
)

type Uploader struct {
	Endpoint string
}

func (u Uploader) Upload(name string, file []byte) {
	var buf bytes.Buffer
	multipart := multipart.NewWriter(&buf)
	writer, err := multipart.CreateFormFile("file", path.Base(name))
	if err != nil {
		log.Printf("Error while creating multipart %v", err)
		return
	}
	writer.Write(file)
	multipart.Close()
	req, _ := http.NewRequest("POST", u.Endpoint, &buf)
	req.SetBasicAuth(user, pass)
	req.Header.Set("Content-Type", multipart.FormDataContentType())
	client := &http.Client{}
	res, err := client.Do(req)
	//res, err := http.Post(u.Endpoint, getContentType(name), reader)
	if err != nil || res.StatusCode != http.StatusOK {
		log.Printf("Error while uploading image, %v\nres, %v", err, res)
	} else {
		log.Printf("Response %s", res.Status)
	}
}

func getContentType(name string) string {
	return "image/" + path.Ext(name)[1:]
}

func init() {
	user = os.Getenv("UPLOAD_USER")
	pass = os.Getenv("UPLOAD_PASSWORD")

	if user == "" || pass == "" {
		panic("Must set the UPLOAD_USER and UPLOAD_PASSWORD.")
	}
}
