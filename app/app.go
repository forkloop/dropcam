package main

import (
	"flag"
	"fmt"
	"os"
	"dropcam"
)

func main() {
	var (
		endpoint string
		dir string
	)

	flag.StringVar(&dir, "d", "", "watch dir")
	flag.StringVar(&endpoint, "e", "http://localhost:4000/tartan", "server endpoint.")
	flag.Parse()

	if dir == "" {
		fmt.Println("Missing dir.")
		os.Exit(1)
	}

	fmt.Printf("start to watch [%v] and upload to [%v]\n", dir, endpoint)
	uploader := dropcam.Uploader{Endpoint: endpoint}
	handler := dropcam.UpdateHandler{Dir: dir, Up: uploader}
	dropcam.WatchDir(handler)

	//apiKey := os.Getenv("MAILGUN_API_KEY")
	//notifier := dropcam.MailgunNotifier{ApiKey: apiKey}
	//notifier.Notify("Watch out, kittens are out!", "forkloop@yahoo.com")
}
