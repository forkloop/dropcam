package dropcam

import (
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path"
	"strings"
	"time"
)

type UpdateHandler struct {
	Dir string // folder name
	Up Uploader
}

func (h UpdateHandler) HandleFile(file string) {
	go func(f interface{}) {
		time.Sleep(1000 * time.Millisecond)
		// upload file to server
		bytes, err := ioutil.ReadFile(file)
		if err != nil {
			log.Printf("Error while reading file %v: %v", file, err)
			return
		}
		h.Up.Upload(file, bytes)
	}(file)
}

func (h UpdateHandler) Handle(t time.Time) {
	output, err := exec.Command("ls", "-rt", h.Dir).Output()
	if err != nil {
		log.Printf("Error while listing files %v", err)
		return
	}

	files := strings.Split(string(output), "\n")
	for _, base := range files {
		file := path.Join(h.Dir, base)
		info, err := os.Stat(file)
		if err != nil {
			continue
		}

		time := info.ModTime()
		// handle file which is newer than the given time
		if time.After(t) {
			go func(f interface{}) {
				// upload file to server
				bytes, err := ioutil.ReadFile(f.(string))
				if err != nil {
					log.Printf("Error while reading file %v: %v", f, err)
					return
				}
				h.Up.Upload(file, bytes)
			}(file)
		}
	}
}
