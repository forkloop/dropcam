package dropcam

import (
	"github.com/howeyc/fsnotify"
	"strings"
	"log"
)

func WatchDir(handler UpdateHandler) {
	watcher, err := fsnotify.NewWatcher()

	if err != nil {
		log.Printf("Error while creating watcher, %v", err)
	}

	done := make(chan bool)

	go func() {
		for {
			select {
			case evt := <-watcher.Event:
				log.Println("event: ", evt)
				// if use evt.IsCreate(), we need to sleep a sec in the Handler goroutine
				// as the file is likely not there yet
				// `touch` will trigger a CREATE event, however, since the file is empty,
				// MODIFY event will not be triggered.
				if evt.IsModify() && !evt.IsAttrib() {
					if strings.HasSuffix(evt.Name, "jpg") {
						handler.HandleFile(evt.Name)
					}
				}
			case err := <-watcher.Error:
				log.Println("error: ", err)
				done <- true
				return
			}
		}
	}()

	err = watcher.Watch(handler.Dir)
	if err != nil {
		log.Fatalln(err)
	}

	defer watcher.Close()

	<-done
}
